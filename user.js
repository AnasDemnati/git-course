'use strict';

const route = require('express').Router();

const notification = require('./notification');
const controller = require('./controller');

route
    .put('/:userId', controller.update)
    .delete('/:userId', controller.delete);

module.exports = route;
